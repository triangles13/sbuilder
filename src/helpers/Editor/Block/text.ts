export default {
    OnlyText: {
        label: 'Текст',
        path: 'components/Editor/Blocks/Text/OnlyText.vue'
    },
    TextWithTitle: {
        label: 'Текст с заголовком',
        path: 'components/Editor/Blocks/Text/TextWithTitle.vue'
    }
} as any
