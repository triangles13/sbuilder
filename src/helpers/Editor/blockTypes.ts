export default {
    Cover: {
        label: 'Обложка',
        path: 'components/Editor/Blocks/Cover.vue'
    },
    SText: {
        label: 'Текст',
        path: 'components/Editor/Blocks/Text.vue'
    },
    SPic: {
        label: 'Изображение',
        path: 'components/Editor/Blocks/Pic.vue'
    }
} as any
