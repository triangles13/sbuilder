class Store {
    public getAll = () => new Promise((resolve, reject) => {
        try {
            const allProjects = [] as any

            Object.keys(localStorage).forEach((projectKey) => {
                if (projectKey.slice(0, 8) === '_Project') {
                    allProjects.push(JSON.parse(localStorage[projectKey]))
                }
            })

            resolve(allProjects)
        } catch (e) {
            reject(e)
        }
    })

    public get = (id: number) => new Promise((resolve, reject) => {
        try {
            const value = localStorage.getItem(`_Project${id}`) as string

            resolve(JSON.parse(value))
        } catch (e) {
            reject(e)
        }
    })

    public post = (data: object, id: number = +new Date()) => new Promise((resolve, reject) => {
        try {
            const stringValue = JSON.stringify({
                ...data,
                id
            })
            localStorage.setItem(`_Project${id}`, stringValue)

            resolve(id)
        } catch (e) {
            reject(e)
        }
    })

    public delete = (id: number) => new Promise((resolve, reject) => {
        try {
            localStorage.removeItem(`_Project${id}`)

            resolve('Success')
        } catch (e) {
            reject(e)
        }
    })
}


const store = new Store()

export default store
