import Vue from 'vue'

export default {
    state: {
        blocks: []
    },
    mutations: {
        set(state: any, blocks: any = []) {
            state.blocks = blocks
        },
        add(state: any, block: any) {
            state.blocks.push({
                id: +new Date(),
                ...block
            })
        },
        copy(state: any, id: number) {
            const getNewElem = (elem: any) => {
                if (typeof elem === 'object') {
                    const newVal = {...elem}

                    Object.keys(newVal).forEach((key: any) => {
                        newVal[key] = getNewElem(newVal[key])
                    })

                    return newVal
                }

                return elem
            }

            const blocks = [...state.blocks]

            state.blocks = blocks.reduce((acc: any, cur: any) => {
                acc.push(cur)

                if (cur.id === id) {
                    const copy = getNewElem(cur)
                    copy.id = +new Date()

                    acc.push(copy)
                }

                return acc
            }, [])
        },
        delete(state: any, id: number) {
            state.blocks = state.blocks.reduce((acc: any, cur: any) => {
                if (cur.id !== id) {
                    acc.push(cur)
                }

                return acc
            }, [])
        },
        up(state: any, index: number) {
            if (index > 0) {
                const blocks = [...state.blocks]
                const current = blocks[index]
                blocks[index] = blocks[index-1]
                blocks[index-1] = current
                state.blocks = blocks
            }
        },
        down(state: any, index: number) {
            if (index + 1 < Object.keys(state.blocks).length) {
                const blocks = [...state.blocks]
                const current = blocks[index]
                blocks[index] = blocks[index+1]
                blocks[index+1] = current
                state.blocks = blocks
            }
        },
        updateProp(state: any, val: any) {
            let elem = state.blocks[val.index]
            const arr = val.prop.split('.')

            arr.forEach((propName: any, index: number) => {
                if (index === arr.length - 1) {
                    Vue.set(elem, propName, val.value)
                } else {
                    if (elem[propName] === undefined) {
                        Vue.set(elem, propName, {})
                    }
                    elem = elem[propName]
                }
            })

            elem = val.value
        }
    },
    namespaced: true
}
