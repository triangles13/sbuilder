export default {
    state: {
        hideField: true
    },
    mutations: {
        changeField(state: any, status: boolean) {
            state.hideField = status
        }
    },
    namespaced: true
}
