export default {
    state: {
        active: null
    },
    mutations: {
        set(state: any, project: boolean) {
            state.active = project
        }
    },
    namespaced: true
}
