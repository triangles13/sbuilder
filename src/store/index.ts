import Vue from 'vue'
import Vuex from 'vuex'

import project from '@/store/modules/project'
import blocks from './modules/blocks'
import controls from './modules/controls'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        project,
        blocks,
        controls
    },
    strict: true
})
