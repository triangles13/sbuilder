import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import storage from '@/helpers/Storage/store'
import startProject from '@/helpers/startProject'

Vue.config.productionTip = false

import { library } from '@fortawesome/fontawesome-svg-core'
import { faTrash, faCopy, faArrowUp, faArrowDown, faArrowLeft, faChevronDown,
  faBold, faItalic, faUnderline, faStrikethrough, faLink, faAlignLeft, faAlignCenter, faAlignRight} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add({faTrash, faCopy, faArrowUp, faArrowDown, faArrowLeft, faChevronDown,
  faBold, faItalic, faUnderline, faStrikethrough, faLink, faAlignLeft, faAlignCenter, faAlignRight})
Vue.component('FAIcon', FontAwesomeIcon)

document.execCommand("defaultParagraphSeparator", false, "p");

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

if (!localStorage.getItem('init')) {
  storage.post(startProject)
  localStorage.setItem('init', '1')
}
